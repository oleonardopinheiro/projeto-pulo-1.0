import requests
import os
import pandas as pd
import json
import urllib.request
from sqlalchemy import create_engine
import cx_Oracle

#TRAZENDO PARA O CÓDIGO O ARQUIVO CONFIG, ONDE ESTÁ PARAMETRIZADO AS CONEXÕES EXTERNAS

with open("./conf/config.json") as json_data_file:
        config = json.load(json_data_file)

#CONFIGURAÇÕES DE CONEXÃO ORACLE CLOUD

client = config['DATABASE']['CLIENT']
cx_Oracle.init_oracle_client(lib_dir=client)
os.environ[config['DATABASE']['TNS']] = config['DATABASE']['ENVIRON']

#CONEXÃO BANCO

connection = cx_Oracle.connect(user=config['DATABASE']['USER'], password=config['DATABASE']['PASSWORD'], dsn=config['DATABASE']['DSN'])
cursor = connection.cursor()

#DELETE TABELA BIPULO_DADO
delete =  "DELETE bipulo_dado"
cursor.execute(delete)
connection.commit()

page = 1
while True:
    #REQUISIÇÃO DA API + NORMALIZAÇÃO DE DATA
    url = config['API']['URL'] + str(page)
    payload = {}
    headers = {
        'Authorization': config['API']['AUTH']
    }

    response = requests.request("GET", url, headers=headers, data=payload, )
    data = json.loads(response.content)
    dados = pd.json_normalize(data['items'], max_level=50)

    #PAGINAÇÃO SE CASO ESTIVER VAZIO PARAR O LOOP
    if dados.empty:
     print('Pagina Vazia!')
     break

    #FORMATAÇÃO DE COLUNAS (TRAZER APENAS O NECESSÁRIO)
    colunas = ['has_media','section.name', 'question.name', 'content_format', 'comment', 'file_url','section.position', 'question.position', 
        'score_weight', 'item_id' ,'section.description', 'attachments']
    formatacao = dados.loc[:, colunas]

    # CRIANDO UMA LISTA DE TUPLAS COM OS DADOS DO DATAFRAME
    values = [tuple(str(x) for x in row) for row in formatacao.values]

    #INICIANDO CONEXÃO COM BANCO DE DADOS ORACLE E INSERÇÃO DE DADOS
    try:
        cursor.executemany("""INSERT INTO bipulo_dado (ID_PULO_DADO, FG_IMAGEM, NM_SECAO, NM_QUESTAO, RESPOSTA, COMENTARIO, URL, ORDEM_SECAO, ORDEM_QUESTAO, PESO, ID_PESQ_WECHECK, DS_QUESTAO, ANEXO) VALUES ('',:0, :1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11)""", values)
        connection.commit()
        print('Dados inseridos no banco Oracle com sucesso!')
    except cx_Oracle.DatabaseError as e:
        print('Error:', e)
        connection.rollback()

    #Paginação
    page+= 1

#ADICIONAR DADOS NO EXCELL, ATIVAR = TRUE, DESATIVAR = FALSE. 
ex = False
if ex == True:
    dataframe = pd.DataFrame(formatacao)
    dataframe.to_excel("PULO - DadosWeCheck.xlsx")
    print('Dados inseridos no Excell com sucesso!')
else:
    print('Inserção de Dados no Excell Desativado!')
